all: cython_wp.pyx cython_p.pyx cython_blocked backward forward lu scilup
	

cython_wp.so:
	cython cython_wp_pyx
	gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I/usr/include/python3.5m -o cython_wp.so cython_wp.c

cython_piv:
	cython cython_p.pyx
	gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I/usr/include/python3.5m -o cython_p.so cython_p.c

cython_blocked:
	cython cython_blocked.pyx
	gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I/usr/include/python3.5m -o cython_blocked.so cython_blocked.c

cyblo:
	cython cython_blocked.pyx
	gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I/usr/include/python3.5m -o cython_blocked.so cython_blocked.c



backward:
	python main.py cython backward --out out/c_backward.json --stop 4000 --unsafe
	python main.py numpy backward --out out/np_backward.json --stop 4000 --unsafe
	python main.py python backward --out out/p_backward.json --stop 4000 --unsafe
	python main.py numba backward --out out/nb_backward.json --stop 4000 --unsafe
	python main.py cython backward --out out/c_backward2.json --stop 4000 --unsafe
	python main.py numpy backward --out out/np_backward2.json --stop 4000 --unsafe
	python main.py python backward --out out/p_backward2.json --stop 4000 --unsafe
	python main.py numba backward --out out/nb_backward2.json --stop 4000 --unsafe
	python main.py cython backward --out out/c_backward3.json --stop 4000 --unsafe
	python main.py numpy backward --out out/np_backward3.json --stop 4000 --unsafe
	python main.py python backward --out out/p_backward3.json --stop 4000 --unsafe
	python main.py numba backward --out out/nb_backward3.json --stop 4000 --unsafe
forward:
	python main.py cython forward --out out/c_forward4.json --stop 6000 --unsafe
	python main.py numpy forward --out out/np_forward4.json --stop 6000 --unsafe
	python main.py python forward --out out/p_forward4.json --stop 6000 --unsafe
	python main.py numba forward --out out/nb_forward4.json --stop 6000 --unsafe

lu:
	python main.py cython lu --out out/c_lu.json --stop 2000 --unsafe
	python main.py numpy lu --out out/np_lu.json --stop 2000 --unsafe
	python main.py python lu --out out/p_lu.json --stop 2000 --unsafe
	python main.py numba lu --out out/nb_lu.json --stop 2000 --unsafe
	python main.py cython lu --out out/c_lu2.json --stop 2000 --unsafe
	python main.py numpy lu --out out/np_lu2.json --stop 2000 --unsafe
	python main.py python lu --out out/p_lu2.json --stop 2000 --unsafe
	python main.py numba lu --out out/nb_lu2.json --stop 2000 --unsafe
	python main.py cython lu --out out/c_lu3.json --stop 2000 --unsafe
	python main.py numpy lu --out out/np_lu3.json --stop 2000 --unsafe
	python main.py python lu --out out/p_lu3.json --stop 2000 --unsafe
	python main.py numba lu --out out/nb_lu3.json --stop 2000 --unsafe

lup:
	python main.py cythonp lu --out c_lup.json
	python main.py numpyp lu --out np_lup.json
	python main.py pythonp lu --out p_lup.json
	python main.py numbap lu --out nb_lup.json
	python main.py cythonp lu --out c_lup2.json
	python main.py numpyp lu --out np_lup2.json
	python main.py pythonp lu --out p_lup2.json
	python main.py numbap lu --out nb_lup2.json
	python main.py cythonp lu --out c_lup3.json
	python main.py numpyp lu --out np_lup3.json
	python main.py pythonp lu --out p_lup3.json
	python main.py numbap lu --out nb_lup3.json

lub:
	python main.py cythonb lub --out c_lub.json
	python main.py cythonb lub --out c_lub2.json
	python main.py cythonb lub --out c_lub3.json

scilup:
	python main.py scipy lu --out out/scipy_lup.json --stop 2000 --unsafe
	python main.py scipy lu --out out/scipy_lup2.json --stop 2000 --unsafe
	python main.py scipy lu --out out/scipy_lup3.json --stop 2000 --unsafe
