import numpy as np
cimport numpy as np
from cpython cimport bool
import scipy.linalg

from cython_p import forward, backward
from util import make_permutation_matrix

DTYPE = np.double

ctypedef np.double_t DTYPE_t

import cython
import scipy.linalg

np.set_printoptions(linewidth=150)

def lu_factorization_u(np.ndarray[DTYPE_t, ndim=2] a, np.ndarray[np.int_t, ndim=1] index, int start, int stop):
    """Factorize a by an unblocked LU-Factorization. 

    Args:
        a : array to factorize
        index : pivoting lookup array
        start : we factorize a[start:stop start:stop]
        stop  : we factorize a[start:stop start:stop]
        
    WARNING: PRODUCES SITE EFFECTS
    """
    
    cdef int l, n, k, p, j, i
    cdef DTYPE_t value
    n =  a.shape[1]
    for k in range(start+1, stop):
        p = k
        value = a[index[k],k]
        for j in range(k+1, n):
            if abs(value) < abs(a[index[j],k]):
                p = j
                value = a[index[j],k]
        if k != p:
            index[k], index[p] = index[p], index[k]
        for i in range(k+1, stop):
            a[index[i],k] = a[index[i],k] / a[index[k],k]
        for i in range(k+1, stop):
            for j in range(k+1, stop):
                a[index[i],j] = a[index[i],j] - (a[index[i],k] * a[index[k],j])
    return a

def lu_factorization(np.ndarray[DTYPE_t, ndim=2] a, int n, int b):
    cdef np.ndarray[np.int_t, ndim=1] index
    cdef int i, j
    index = np.arange(n, dtype=np.int)

    a2 = lu_factorization_u(a, index, 0, n)

    p = make_permutation_matrix(index)
    print(p @ a - ((np.tril(a2, -1) + np.eye(n,n)) @ np.triu(a2)))

    for i in range(0, n, b):
        a2 = lu_factorization_u(a.copy(), index, i, i+b)

        for j in range(i, n):
            forward(a[index[i:i+b], i:i+b], a[index[i:b+i], j], a[index[i:b+i], j], b, True)
            # tranposing does not use much time, because it is implemented as views
            forward(a[index[i:i+b], i:i+b].transpose(), a[index[j], i:i+b].transpose(), a[index[j], i:i+b].transpose(), b)

        a[index[i+b:n], i+b:n] = a[index[i+b:n], i+b:n] - a[index[i+b:n], i:i+b] @ a[index[i:i+b], i+b:n]
    return(a, index)
