import numpy as np
cimport numpy as np
from cpython cimport bool
from util import make_permutation_matrix

DTYPE = np.double

ctypedef np.double_t DTYPE_t

import cython


print("HELLO!")
@cython.boundscheck(False)
def forward(np.ndarray[DTYPE_t, ndim=2] a, np.ndarray[DTYPE_t, ndim=1] b, np.ndarray[DTYPE_t, ndim=1] x, int n, bool unit_diagonal=False):
    """Calculate forward substituion of a lower triangular system

    Args:
        a -  Matrix 
        b - Residuum
        x - empty initialized array that will hold the solution
        N - dimension

    Returns:
        x - solution to "a @ x = b"
    """
    cdef int i, j, k
    if unit_diagonal:
        for i in range(0, n):
            #x[i] = b[i]
            #b[i+1:n] = b[i+1:n] - (a[i+1:n, i] * x[i])
            for j in range(i+1, n):
                b[j] = b[j]-(a[j,i]*x[i])
        return(x)
    else:
        for i in range(0, n):
            if a[i,i] == 0:
                break
            #x[i] = b[i]/a[i,i]
            #b[i+1:n] = b[i+1:n] - (a[i+1:n, i] * x[i])
            for j in range(i+1, n):
                b[j] = b[j]-a[j,i]*x[i]
        return(x)

@cython.boundscheck(False)
def backward(np.ndarray[DTYPE_t, ndim=2] a, np.ndarray[DTYPE_t, ndim=1] b, np.ndarray[DTYPE_t, ndim=1] x, int n):
    # the second argument is non inclusive, so to get a range [n-1, n-2, ... 1, 0]
    # we need to pass -1 to range
    cdef int i, j
    for j in range(n-1, 0-1, -1):
        if a[j,j] == 0:
            break
        x[j] = b[j] / a[j,j]
        for i in range(0, j):
            b[i] = b[i] - a[i,j] * x[j]
    return x


def lu_factorization(np.ndarray[DTYPE_t, ndim=2] a, int n):
    cdef int k, p, j, i
    cdef np.ndarray[np.int_t, ndim=1] index
    cdef DTYPE_t value
    cdef np.ndarray[DTYPE_t, ndim=2] a2
    cdef np.ndarray[DTYPE_t, ndim=2] pp

    n = a.shape[0]
    index = np.arange(n)
    for k in range(0, n-1):
        p = k
        value = abs(a[index[k],k])
        for i in range(k+1, n):
            if abs(a[index[i], k]) > value:
                p = i
                value = abs(a[index[i], k])
        index[k], index[p] = index[p], index[k]
        for i in range(k+1, n):
            a[index[i],k] = a[index[i],k] / a[index[k],k]
        for j in range(k+1, n):
            for i in range(k+1, n):
                a[index[i],j] = a[index[i],j] - (a[index[i],k] * a[index[k],j])

    return a, index
