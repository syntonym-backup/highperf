import numpy as np
cimport numpy as np
from cpython cimport bool

DTYPE = np.double

ctypedef np.double_t DTYPE_t

import cython


@cython.boundscheck(False)
def forward(np.ndarray[DTYPE_t, ndim=2] a, np.ndarray[DTYPE_t, ndim=1] b, np.ndarray[DTYPE_t, ndim=1] x, int n, bool unit_diagonal=False):
    """Calculate forward substituion of a lower triangular system

    Args:
        a -  Matrix 
        b - Residuum
        x - empty initialized array that will hold the solution
        N - dimension

    Returns:
        x - solution to "a @ x = b"
    """
    cdef int i, j, k
    for i in range(0, n):
        if a[i,i] == 0:
            break
        x[i] = b[i]/a[i,i]
        for j in range(i+1, n):
            b[j] = b[j]-a[j,i]*x[i]
    return(x)

@cython.boundscheck(True)
def backward(np.ndarray[DTYPE_t, ndim=2] a, np.ndarray[DTYPE_t, ndim=1] b, np.ndarray[DTYPE_t, ndim=1] x, int n):
    # the second argument is non inclusive, so to get a range [n-1, n-2, ... 1, 0]
    # we need to pass -1 to range
    cdef int i, j
    for j in range(n-1, 0-1, -1):
        if a[j,j] == 0:
            break
        x[j] = b[j] / a[j,j]
        for i in range(0, j):
            b[i] = b[i] - a[i,j] * x[j]
    return x

@cython.boundscheck(False)
def lu_factorization(np.ndarray[DTYPE_t, ndim=2] a, int n):
    cdef int k, i, j
    for k in range(0,n):
        for i in range(k+1, n):
            a[i,k] = a[i,k] / a[k,k]
        for j in range(k+1, n):
            for i in range(k+1, n):
                a[i,j] = a[i,j] - (a[i,k] * a[k,j])

    return(a)
