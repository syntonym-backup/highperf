import json
import sys
import click
import time

import util
import measure

class FailFast():
    pass

import python_wp
import numpy_wp
import cython_wp
import cython_blocked

import python_p
import cython_p
import numpy_p 
import scipy.linalg


try:
    import numba_wp
    import numba_p
except:
    numba_wp = FailFast()

module_mapping =  {
        "python": python_wp,
        "numpy": numpy_wp,
        "cython": cython_wp,
        "numba": numba_wp,
        "pythonp": python_p,
        "numpyp": numpy_p,
        "cythonp": cython_p,
        "numbap": numba_p,
        "scipy": scipy.linalg,
        "cythonb": cython_blocked,
        }

function_mapping = {
        "lu": "lu_factorization",
        "forward": "forward",
        "backward": "backward",
        "lub": "lu_factorization",
        }

measure_mapping = {
        "lu": measure.measure_lu_factorization,
        "forward": measure.measure_forward,
        "backward": measure.measure_backward,
        "lub": measure.measure_lu_blocked,
        }

def test_implementation(module, function, timeout, total_timeout, results, start=100, step=100, end=10000):
    """test an implementation

    module        (str)       - name of module to test. 
                                See `module_mapping` for valid names.
    function      (str)       - name of function to test. 
                                See `measure_mapping` for valid names.
    timeout       (int/float) - how long a single test may need
    total_timeout (int/float) - after this time the test is aborted
    """
    if module == "scipy":
        implementation = scipy.linalg.lu
        measure_function = measure.measure_lu_scipy
        module = scipy.linalg
    else:
        module  = module_mapping[module]
        implementation = getattr(module, function_mapping[function])
        measure_function = measure_mapping[function]


    total_time = 0
    single_call_time = 0
    n = start
    while n <= end and total_time <= total_timeout and single_call_time<=timeout:
        start_time = time.time()
        measure_function(results, n, implementation)
        single_call_time = results[-1]["time"]
        n += step
        total_time += start_time - time.time()
        sys.stdout.write(".", )
        sys.stdout.flush()
    sys.stdout.write("\n")

@click.command()
@click.argument("module")
@click.argument("function")
@click.option("--timeout", default=10, help="measure until a function call takes timeout time")
@click.option("--total_timeout", default=20*60, help="measure until a total time of total_timeout is reached")
@click.option("--start", default=100, help="start dimension")
@click.option("--step", default=100, help="stepsize of dimension")
@click.option("--stop", default=12000, help="stop dimension")
@click.option("--out", default="out.json", help="outfile to write to")
@click.option("--safe/--unsafe", default=True, help="append or write to out") 
def main(module, function, timeout, total_timeout, start, step, stop, out, safe):
    """Measure how fast and with what accuracy `function` is executed.
    There are different implementations in different modules.
    """
    results = []
    try:
        test_implementation(module, function, timeout, total_timeout, results, start, step, stop)
    except KeyboardInterrupt:
        pass
    finally:
        if safe:
            o = "a"
        else:
            o = "w"
        with open(out, o) as f:
            f.write(json.dumps(results))


if __name__ == "__main__":
    main()
