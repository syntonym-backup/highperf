import json
import click
import math
from functools import reduce
from bokeh.plotting import figure, output_file, show
from path import path
import time

extract = "rel_residual_norm"

def consume(name, func):
    i = 1
    dims = []
    times = []
    while True:
        if i == 1:
            number_suffix = ""
        else:
            number_suffix = i

        i += 1

        file_path = path("{}_{}{}.json".format(name, func, number_suffix))

        if not file_path.exists():
            break

        print(file_path)
        with file_path.open() as f:
            inp = f.read()

        try:
            a = json.loads(inp)
        except:
            print("Error when reading {}".format(str(file_path)))
            raise

        dims.append([k["dim"] for k in a])
        times.append([float(k[extract]) for k in a])

    length = len(dims)

    return_dims = [reduce(lambda x,y: x+y, tup, 0)/length for tup in zip(*dims)]
    return_time = [(reduce(lambda x,y: x+y, tup, 0)/length) for tup in zip(*times)]

    return(return_dims, return_time)

def make_graph(func_name):

    #dim1, time1 = consume("scipy", "lup")
    #dim2, time2 = consume("nb", "lup")
    #dim3, time3 = consume("c", "lub")
    #dim_numba_backward, time_numba_backward = consume("nb", func_name)
    #dim_scipy_backward, time_scipy_backward = consume("scipy", func_name)
    #dim_numpy_backward, time_numpy_backward = consume("np", func_name)
    dim_cython_backward, time_cython_backward = consume("c", func_name)
    #dim_python_backward, time_python_backward = consume("p", func_name)

    output_file("{}_{}.html".format(func_name, time.time()), title="LU-Factorization with partial pivoting".format(func_name.capitalize()))

    p = figure(title="LU-Factorization with partial pivoting".format(func_name.capitalize()), x_axis_label="input dimension", y_axis_label="rel. forward error")

    #p.line(dim1, time1, line_color="green", legend="scipy reference")
    #p.line(dim2, time2, line_color="red", legend="unblocked with partial pivoting")
    #p.line(dim3, time3, line_color="orange", legend="blocked with partial pivoting")
    #p.line(dim_scipy_backward, time_scipy_backward, line_color="orange")
    #p.line(dim_python_backward, time_python_backward, legend="pure python", line_width=5, line_dash="dashed")
    #p.line(dim_numba_backward, time_numba_backward, legend="numba", line_color="orange", line_width=1)
    #p.line(dim_numpy_backward, time_numpy_backward, legend="numpy", line_color="red", line_dash="dotted", line_width=5)
    p.line(dim_cython_backward, time_cython_backward, legend="cython", line_color="green", line_width=5)

    show(p)

@click.command()
@click.argument("func_name", default="forward")
def main_cli(func_name):
    make_graph(func_name)

if __name__ == "__main__":
    main_cli()
