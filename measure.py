"""
Various measuring functions. All functions take a list `results` and append the measured results. `n` determines the dimension.
All functions will be seeded their appropriate data. It is thus important to call the right function, e.g. measure_forward will
create a random lower triangular matrix of dimension n times n. The API of the functions should be the same as in the example code.
"""
from util import seed, calc_rel_residual_norm, calc_rel_forward_error, calc_rel_factorization_error, make_permutation_matrix

import time
import numpy as np

def measure(function, *args, **kwargs):
    """Measure how long `function` takes if executed with *args and **kwargs

    Returns:
        (time_delta, result) - (how long it took, the result of the function call)
    """
    start = time.time()
    x = function(*args, **kwargs)
    end = time.time()
    return(end-start, x)

def measure_forward(results, n, forward):
    """Measure the function forward and add the results to the `results` list.

    Args:
        results (list)     - list to add results to
        n (int)            - dimension of problem
        forward (function) - function to calculate forward substitution
    """
    lower, x, b, empty = seed("lower", n)

    t_diff, result = measure(forward, lower.copy(), b.copy(), empty.copy(), n)

    results.append({"name": "forward",
        "rel_forward_error": calc_rel_forward_error(result.copy(), x.copy(), lower.copy(), b.copy()),
        "rel_residual_norm": calc_rel_residual_norm(result.copy(), x.copy(), lower.copy(), b.copy()),
        "time": t_diff,
        "dim": n})

def measure_backward(results, n, backward):
    """Measure the function `backward` and add the results to the `results` list.

    Args:
        results (list)      - list to add results to
        n (int)             - dimension of problem
        backward (function) - function to calculate backwards substitution
    """
    upper, x, b, empty = seed("upper", n)
    t_diff, result = measure(backward, upper.copy(), b.copy(), empty.copy(), n)

    results.append({"name": "backward",
        "rel_forward_error": calc_rel_forward_error(result.copy(), x.copy(), upper.copy(), b.copy()),
        "rel_residual_norm": calc_rel_residual_norm(result.copy(), x.copy(), upper.copy(), b.copy()),
        "time": t_diff,
        "dim": n})

def measure_lu_factorization(results, n, lu_factorization):
    """Measure the function `lu_factorization` and add the results to the `results` list.

    Args:
        results (list)              - list to add results to
        n (int)                     - dimension of problem
        lu_factorization (function) - function to calculate lu factorization
    """

    matrix, x, b, empty = seed("normal", n)

    tdiff, result = measure(lu_factorization, matrix.copy(), n=n)
    if len(result) == 2:
        pivoting = True
        result, p = result
        result = result[p, :]
    else:
        pivoting = False

    l = np.tril(result, -1) + np.eye(n, n)
    u = np.triu(result)


    
    if pivoting:
        original_matrix = matrix[p, :].copy()
    else:
        original_matrix = matrix.copy()

    results.append({"name": "lu_factorization",
        "rel_factorization_error": calc_rel_factorization_error(original_matrix, l.copy(), u.copy()),
        "time": tdiff,
        "dim": n})

def measure_lu_blocked(results, n, lu_factorization):
    """Measure the function `lu_factorization` and add the results to the `results` list.

    Args:
        results (list)              - list to add results to
        n (int)                     - dimension of problem
        lu_factorization (function) - function to calculate lu factorization
    """

    matrix, x, b, empty = seed("normal", n)

    tdiff, result = measure(lu_factorization, matrix.copy(), n=n, b=n)

    if len(result) == 2:
        pivoting = True
        result, p = result
        result = result[p, :]
        matrix = matrix[p,:]

    l = np.tril(result, -1) + np.eye(n, n)
    u = np.triu(result)

    results.append({"name": "lu_blocked",
        "rel_factorization_error": calc_rel_factorization_error(matrix.copy(), l.copy(), u.copy()),
        "time": tdiff,
        "dim": n})

def measure_lu_scipy(results, n, lu_factorization):
    """Measure the function `lu_factorization` and add the results to the `results` list.

    Args:
        results (list)              - list to add results to
        n (int)                     - dimension of problem
        lu_factorization (function) - function to calculate lu factorization
    """

    matrix, x, b, empty = seed("normal", n)

    tdiff, (l, u) = measure(lu_factorization, matrix.copy(), permute_l=True)

    results.append({"name": "lu_factorization",
        "rel_factorization_error": calc_rel_factorization_error(matrix, l, u),
        "time": tdiff,
        "dim": n})

def measure_solve(results, n, solve):
    """Measure the function `lu_factorization` and add the results to the `results` list.

    Args:
        results (list)   - list to add results to
        n (int)          - dimension of problem
        solve (function) - function to solve a linear system
    """
    
    matrix, x, b, empty = seed("normal", n)

    t_diff, result = measure(solve, matrix, b, n=n)

    results.append({"name": "solve",
        "rel_forward_error": calc_rel_forward_error(result, x, lower, b),
        "rel_residual_norm": calc_rel_residual_norm(result, x, lower, b),
        "time": t_diff,
        "dim": n})
