import json
import numpy as np

with open("results.json") as f:
    inp = f.read()

a = json.loads(inp)


acc = [k["rel_residual_norm"] for k in a]

avg = np.average(acc)

print(avg)
