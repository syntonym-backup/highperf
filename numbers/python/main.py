import json
from bokeh.plotting import figure, output_file, show

PYTHON=False
NUMPY=False
CYTHON=False
SCIPY=True

to_examine = "rel_residual_norm"

output_file("backward.html", title="lu factorization")

p = figure(title="forward substitution", x_axis_label="input dimension", y_axis_label="rel. residual norm")

if PYTHON:
    with open("results.json") as f:
        inp = f.read()

    a = json.loads(inp)

    dims = [k["dim"] for k in a]
    time = [k[to_examine] for k in a]
    p.line(dims, time, legend="python_lu_factorization")

if NUMPY:
    with open("results_numpy_forward.json") as f:
        inp = f.read()

    a = json.loads(inp)

    dim_numpy_forward = [k["dim"] for k in a]
    time_numpy_forward = [k[to_examine] for k in a]
    p.line(dim_numpy_forward, time_numpy_forward, legend="numpy_lu_factorization", line_color="red")

if CYTHON:
    with open("results_cython_lu_factorization.json") as f:
        inp = f.read()

    a = json.loads(inp)

    dim_cython_forward = [k["dim"] for k in a]
    time_cython_forward = [k[to_examine] for k in a]
    p.line(dim_cython_forward, time_cython_forward)

if SCIPY:
    with open("results_scipy_forward.json") as f:
        inp = f.read()

    a = json.loads(inp)

    dim_scipy_forward = [k["dim"] for k in a]
    time_scipy_forward = [k[to_examine] for k in a]
    p.line(dim_scipy_forward, time_scipy_forward)






show(p)
