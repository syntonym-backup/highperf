import numpy as np
import scipy.linalg
import numba

def forward(a, b, x, n, unit_diagonal=False):
    """Calculate forward substituion of a lower triangular system

    Args:
        a -  Matrix 
        b - Residuum
        x - empty initialized array that will hold the solution
        n - dimension

    Returns:
        x - solution to "a @ x = b"
    """
    if unit_diagonal:
        for i in range(0, n):
            x[i] = b[i]
            for j in range(i+1, n):
                b[j] = b[j]-a[j,i]*x[i]
        return(x)
    else:
        for i in range(0, n):
            if a[i,i] == 0:
                break
            x[i] = b[i]/a[i,i]
            for j in range(i+1, n):
                b[j] = b[j]-a[j,i]*x[i]
        return(x)

def backward(a, b, x, n):
    # the second argument is non inclusive, so to get a range [n-1, n-2, ... 1, 0]
    # we need to pass -1 to range
    for j in range(n-1, 0-1, -1):
        if a[j,j] == 0:
            break
        x[j] = b[j] / a[j,j]
        for i in range(0, j):
            b[i] = b[i] - a[i,j] * x[j]
    return x

def lu_factorization_u(a, index, o):
    l, n = a.shape
    l, n = l-1, n-1
    for k in range(0, n):
        p = k
        value = a[index[k]-o,k]
        for j in range(k+1, n):
            if abs(value) < abs(a[index[j]-o,k]):
                p = j
                value = a[index[j]-o,k]
        if k != p:
            index[k], index[p] = index[p], index[k]

        for i in range(k+1, n):
            a[index[i]-o,k] = a[index[i]-o,k] / a[index[k]-o,k]
        for i in range(k+1, n):
            for j in range(k+1, l):
                a[index[i]-o,j] = a[index[i]-o,j] - (a[index[i]-o,k] * a[index[k]-o,j])
    return(a, index) 

def lu_factorization(a, n, b):
    index = np.arange(n)
    for i in range(0, n, b):
        lu_factorization_u(a[i:n, i:int(i+b)], index[i:n], i)
    scipy.linalg.solve(a[i:i+b, i:i+b], a[i:i+b, i+b:n], overwrite_b=True)
    a[i+b:n, i+b:n] -= a[i+b:n, i:i+b] @ a[i:i+b, i+b:n]

    return(a)


def give_lu(a, index):
    l, u = np.tril(a, -1) + np.eye(*a.shape), np.triu(a)

    perm = np.zeros(l.shape)
    for j, i in enumerate(index):
        perm[i,j] = 1

    return l,u,perm



