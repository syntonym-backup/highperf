import numpy as np

def forward(a, b, x, n, unit_diagonal=False):
    """Calculate forward substituion of a lower triangular system

    Args:
        a -  Matrix 
        b - Residuum
        x - empty initialized array that will hold the solution
        n - dimension

    Returns:
        x - solution to "a @ x = b"
    """
    if unit_diagonal:
        for i in range(0, n):
            x[i] = b[i]
            for j in range(i+1, n):
                b[j] = b[j]-a[j,i]*x[i]
        return(x)
    else:
        for i in range(0, n):
            if a[i,i] == 0:
                break
            x[i] = b[i]/a[i,i]
            for j in range(i+1, n):
                b[j] = b[j]-a[j,i]*x[i]
        return(x)

def backward(a, b, x, n):
    # the second argument is non inclusive, so to get a range [n-1, n-2, ... 1, 0]
    # we need to pass -1 to range
    for j in range(n-1, 0-1, -1):
        if a[j,j] == 0:
            break
        x[j] = b[j] / a[j,j]
        for i in range(0, j):
            b[i] = b[i] - a[i,j] * x[j]
    return x


def lu_factorization(a, n):
    for k in range(0,n):
        for i in range(k+1, n):
            a[i,k] = a[i,k] / a[k,k]
        for j in range(k+1, n):
            for i in range(k+1, n):
                a[i,j] = a[i,j] - (a[i,k] * a[k,j])

    return(a)

