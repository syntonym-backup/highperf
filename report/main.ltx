\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[naustrian, english]{babel} % deutsche Sprachunterstuetzung
\usepackage{amsmath}
\usepackage{amsfonts}         % math. Schriftarten
\usepackage{amsthm}           % Umgebungen wie proof
\usepackage{amssymb}          % mathematische Sonderzeichen
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\graphicspath{{./images/}}

\begin{document}
\title{Implementation of a High Performance Dense Linear Solver}
\author{Simon Haendeler \\ a1268476}

\maketitle

\section*{Introduction}

This is the report on the assignment `High Performance Dense Linear Solver' of the course `Computer Architecture and High Performance Computing'
in the University of Vienna. The goal is to implement and measure a high performance LU-factorization. 
In a first part a solver without partial pivoting is implemented. Then partial pivoting is added which we will see adds some performance penalty 
but enhances accuracy. In a third step a blocked variant of the LU-factorization is examined.

My programming language of choice is python, which is of the date of this writing often used for scientific purposes.
The default implementation CPython is rather slow, so that it's unsuited for any application targeting high performance. 
Fortunately there are multiple possibilities to speed things up.
CPython has a C-API which makes it possible to seamlessly call compiled C libraries. 
That means the key to high performance in python is to write critical parts in a lower, 
more performant language and then compose the application out of library calls to these performant functions.
This is not unlikely to other languages in which it is better to call optimized libraries like BLAS instead of creating your own implementations.

Other implementations of python often have a better performance than CPython.
But most of them can't call easily into the optimized (C) libraries and are thus unsuited for high performance.
Because of this I will not discuss them here, so anytime I reference python the default implementation CPython is meant.

There exist multiply libraries for high perfomance for python.
Most notably is numpy, which offers an efficient implementation of arrays which the default python lacks.
Nearly all other libraries build on or can be integrated with it. Furthermore vector and array operations are implemented in performant C.
The other libraries I will test are cython and numba which offer to compile python to a more performant language. 
Cython compiles cython code (python with additional type information) to C code, which can then be compiled to native libraries.
These can then seamlessly be imported into python. Numba is JIT compiler which uses LLVM as a backend and compiles python code on the fly to native code.

As a comparison I use the scipy library, which for our purposes can be seen as a wrapper around lapack and blas.
Because it also utilizes numpy arrays and is called from the python runtime the overhead should be comparable.

\section{Methodology}

For a solver two metrics are important: performance and accuracy.

Measuring performance is normally done by giving a predefined task and take the time the measured system needs to solve it.
Time measured can be in wall clock time or processor time.
While processor time is normally more precise wall clock time is normally accurate enough.
It is though more vulnerable to disturbances, thus it is important to control them.
E.g.\ if there are other programs running on the server they might take some processor time and add to the wall clock time although the measured algorithm didn't run.
Even if other applications are kept to a minimum there are other influences that are hard to control.
Kernel internals like processing IP packages can normally not be turned off.
Although the influence should normally be small it is important to understand that the measurement will always involve a random component.
Statistical methods can help against these problems.
Under normal circumstances using multiple measurements and averaging them will lead to a stable enough result.
This is exactly what is done here. Details can be found in the Makefile (generation of multiple datasets) and make\_numbers.py
(processing data).

An additional source of error is the accuracy of the time measuring device.
Normal calls to the system time are not more accurate than milliseconds. It is thus important to 
give large enough work packages such that measurements are not in the milliseconds range but rather in the seconds.
In our case the python module `time' is used which uses the system clock which offers enough accuracy for our case.
Note that on windows accuracy of system time might only be in the seconds.

In detail the time measuring works as follows:
Before executing the function we note the system time. After function evaluating we note the system time.
The total time is then the difference between these two. This method is not one hundred percent exact, e.g.\
the lookup time for the function name is also measured. But because we are especially interested in the relation between two different methods
this overhead does not matter. Also the actual time wasted should be tiny compared to the long time it takes to process big workloads.
This is another reason to measure big work packets instead of doing micro benchmarks. Time measurement code can be found in measure.py.

Accuracy is evaluated by comparing the result the algorithm calculated and the known real result.
To know the result beforehand it is important to craft the problem in such a way that we know the result.
That means we try to get a upper and a lower triangular matrix such that $LU = A$.
Thus $$||LU-A||$$ is an appropriate metric to measure how good we could construct $L$ and $U$.

In the case of the forward respectively backward substitution we try to solve $Ax = b$.
If we create a random (triangular) $A$ we can plug in a known $x$ vector to calculate $b$.
In that way we get a system which solution $x$ we know.
To evaluate accuracy we use the relative forward error and the relative residual norm. These are

$$\frac{||x-x'||}{||x'||}$$ 

and respectively 

$$\frac{||Ax-b||}{||A||\ ||x||}$$

We have to be careful that the random matrices we generate are not ill-conditioned. Otherwise any accuracy is practical meaningless because it is not dominated by the
quality of our algorithm but by the condition of the matrix.
Randomly generated triangular matrices are ill-conditioned and should not be used.
Instead one can generate complete matrices and then use the LU-factorization to get lower
and upper triangular matrices which are not ill-conditioned. Details for data generation can be found in `util.seed'.
Additionally one could take the condition of the matrix into account. 

The choice of the norm is also important. 
For the sake of comparison we take here the one-norm which is defined as follows:

$$\max_k (\sum_j a_{j,k})$$

The code for each implementation is provided or can additionally be downloaded at syntonym.de/highperf.
There are four interesting files for the assignments 1 and 2: python\_wp, cython\_wp, numpy\_wp, numba\_wp which contain the implementation of the specified library
without pivoting. Respectively python\_p, cython\_p, numpy\_p and numba\_p contain the implementation with pivoting.

All logarithmic plots are given in the natural logirithm. The real result is obtained by $e^x$.

\section{Results}

First we are going to discuss the backward and forward substitution.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{forward.png}
	\caption{Various implementations of the forward substitution.
		Note that the time is $\sqrt{t}$ so we can better see 
		that performance is $\mathcal{O}(n^2)$
	}
\end{figure}

Note that in the plot the square root of the time is shown.
That way we can clearly see that the performance is in $\mathcal{O}(n^2)$ just as expected.
The pure python version is very slow and is not even reasonable for large problem sizes. 
The other versions are roughly the same, whereas numpy seems to be a bit slower than numba and cython.
In the numpy version only the inner loop is collapsed to a numpy array notation, which might
explain the difference. In each loop iteration numpy needs to call into python
whereas cython and numba can compute everything in one run without communicating with python.
More efficient versions surely exist.
In the numba version the first run is slower as it should be.
This is due to the JIT which kicks in the first time the function is executed.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{rel_forward_error}
	\includegraphics[width=0.4\textwidth]{resinorm_forward}
	\caption{Accuracy results of the forwards substitution.
		The first plot shows the relative forward error.
		In the second plot we see the residual norm.
		}

\end{figure}

In these figures we can see the accuracy results of the forward substitution.
Because all implementations use the same algorithm and the same datatypes, the accuracy is the same for all of them.
Differences are due to randomness of the data.
Unfortunately the cython backward implementation has a bug I couldn't fix in time.
This further diminishes the validity of the cython LU-Factorization data.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{backward.png}
	\caption{Various implementations of the backward substitution.
		Again the time is $\sqrt{t}$ so we can better see 
		that performance is $\mathcal{O}(n^2)$
	}
\end{figure}

The backward figure is essentially the same as the forward figure.
This is expected as a forward substitution is computationally the same as a backward substitution.
Interestingly the numba version is doing a bit better than before.
It is now a bit faster than the cython version, although practically they share the same source code.
Maybe LLVM can optimize something a bit better than in the forward substitution.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{rel_backward_error}
	\includegraphics[width=0.4\textwidth]{resinorm_backward}
	\caption{Accuracy results of the backward substitution.
		The first figure shows the relative forward error which is in average E-14.
		In the second figure we see the residual norm which is in average E-17.}
\end{figure}

The accuracy results for the backward substitution are comparable to the forward substitution.
Again this is not a surprise.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{lu.png}
	\caption{Various implementations of the LU-Factorization.
		The time is given in $t^{\frac{1}{3}}$ so we can see that performance is
		$\mathcal{O}(n^3)$.
	}
\end{figure}

The LU-Factorization is a $\mathcal{O}(n^3)$ algorithm which we can also see in the figure.
This is also the reason why we can only input much lower problem sizes. 
Cython and numba output appear a bit more random than the numpy version.
Again we can see some spikes, but this time only in the cython and numba versions.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{lu_scipy.png}
	\caption{Various implementations of the LU-Factorization compared to scipy (lapack).
		Note that the scipy call does currently more than our version (partial pivoting and probably blocked algorithm)
		so that this is not a ``fair'' comparison.}
\end{figure}

Most interestingly to us is how we perform in comparison to an established high performance library.
Unfortunately we are doing rather bad. On the other hand the scipy version does (probably) use a blocked algorithm
so that we can't expect to reach the performance with an unblocked one.
Additionally partial pivoting is done, so that even accuracy is higher.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{lu_numba_scipy}
	\caption{Custom LU-Factorization with partial pivoting implementation vs lapack}
\end{figure}

Comparison to scipy shows that our implementation is very slow. Scipy is 21x faster.
This is a bit surprising because earlier results showed comparable speed.
I can't explain why all of a sudden such a big gap is showing.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{pivot_vs_without}
	\includegraphics[width=0.4\textwidth]{lu_accuracy_with_without}
	\caption{In the first figure we see performance of the algorithm with and without pivoting. 
		In the seconds figure we see accuracy given in $\log(error)$}.
\end{figure}

Comparing the versions with pivoting and without pivoting we can see that pivoting does indeed add some time, but not too much.
Accuracy on the other hand is much better with pivoting and is also more stable. The averagy error with pivoting is e-12, without e-9.

The final assignment is to implement a blocked LU-Factorization.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{blocked}
	\caption{The figure shows the time for the unblocked with partial pivoting, blocked with partial pivoting and 
		a scipy (lapack) reference call. We can see that blocking increased our performance substantially but we
	are still far away from scipy.}
\end{figure}

For various reasons only a cython version is provided. A pure python implementation can't compute matrix multiplications effeciently, so a 
blocked version does not add much performance. Numba couldn't handle the BLAS call I used for matrix mulitplication.

But we can see that the blocked version has 4 times better perfomance than the unblocked version. 
The optimized lapack version is again 5 times faster.

\end{document}
