import numpy as np
import python_blocked
import scipy.linalg

a = np.random.random((4,4))

print(python_blocked.lu_factorization(a.copy(), 4, 2))

print(scipy.linalg.lu(a.copy()))

