import numpy as np
import scipy
import scipy.linalg

from functools import partial

norm = partial(np.linalg.norm, ord=1)

def calc_rel_forward_error(x, x_real, A, b):
    return(norm(x - x_real) / norm(x_real))

def calc_rel_residual_norm(x, x_real, A, b):
    return((norm(np.dot(A, x) - b)) / (norm(A) * norm(x)))

def calc_rel_factorization_error(a, l, u):
    return(norm(a - np.dot(l, u)))

def solve(A, b, n, forward, backward, lu_factorization):
    lu_combined = lu_factorization(A)
    y = np.zeros(n)
    x = np.zeros(n)
    #L = np.tril(lu_combined, -1) + np.eye(n,n)
    y = forward(lu_combined, b, y, n, unit_diagonal=True)
    x = backward(lu_combined, b, x, n)
    return(x)

def seed_lu(dim):
    """Create a lower and upper triangular matrix of a given dimension.

    Args:
        dim (int) - matrices will be of dimensions dim²

    Returns:
        l, u (np.array) - lower/upper triangular matrix

    randomly created triangular matrices are not well conditioned.
    Creating first a complete random matrix and then using lu factorization
    avoids this problem.

    The 
    """

    random_matrix = seed("normal", dim, 0)
    l, u = scipy.linalg.lu(random_matrix, permute_l=True, overwrite_a=True, check_finite=False)

    return l,u

def seed(type_, dim, eye_strength=0):
    """Create a random system to solve
    
    Args:
        type_ (str)  - determines wether it's a lower or upper triangular matrix. Must be 'lower', 'upper' or "normal"
        dim (int)    - dimension of system
        eye_strength - factor of the maindiagonal to make the matrix diagonaldominant
        
    Returns:
        triangular (np.Array[dim,dim]) - lower or upper triangular random matrix or normal matrix
        x (np.Array[dim])              - solution you are looking for. Don't use in computations but only to compare
        b (np.array[dim])              - triangular @ x = b 
        empty (np.array[dim])          - initilaized zeroed array to write the solution into.
        """
    if not type_ in ["lower", "upper", "normal"]:
        raise ValueError("type_ must be lower or upper")
    random_A = np.eye(dim, dim) * eye_strength + np.random.rand(dim, dim)
    if type_ in ["lower", "upper"]:
        p, l, u= scipy.linalg.lu(random_A)
    if type_ == "lower":
        triangular = l
    elif type_ == "upper":
        triangular = u
    elif type_ == "normal":
        triangular = random_A

    x = np.ones(dim)
    b = np.dot(triangular, x)

    empty = np.zeros(dim)

    return((triangular, x, b, empty))

def make_permutation_matrix(vector):
    a = np.zeros((*vector.shape, *vector.shape))
    for i, j in enumerate(vector):
        a[i,j] = 1
    return a
